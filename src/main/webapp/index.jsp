<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix = "fmt" %>
<!DOCTYPE html>
<html>
   <head>
      <meta charset="UTF-8">
      <title>Servlet</title>
   </head>
   <body>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
      <script type="text/javascript">
         function handleClick(methodName)
         {
            $.ajax({type : methodName,
                    url : 'action',
                    contentType: 'application/json',
                    success : function(data, status, xhr){
                        window.location.reload();
                    }
                   });
         }
      </script>
      <fmt:setBundle basename="Message" var="msg"/>
      <form id="user-form">
         <table border="1">
            <caption>Buttons</caption>
            <tr>
               <th>Button</th>
               <th>State</th>
            </tr>
            <tr>
               <td>
                  <input type="button"value="GET" onclick="handleClick('get');"/>
               </td>
               <td>
                  <c:if test="${getState}">
                     <fmt:message key="key.on" bundle="${msg}"/>
                  </c:if>
                  <c:if test="${!getState}">
                     <fmt:message key="key.off" bundle="${msg}"/>
                  </c:if>
               </td>
            </tr>
            <tr>
               <td>
                  <input type="button"value="PUT" onclick="handleClick('post');"/>
               </td>
               <td>
                  <c:if test="${postState}">
                     <fmt:message key="key.on" bundle="${msg}"/>
                  </c:if>
                  <c:if test="${!postState}">
                     <fmt:message key="key.off" bundle="${msg}"/>
                  </c:if>
               </td>
            </tr>
            <tr>
               <td>
                  <input type="button"value="PUT" onclick="handleClick('put');"/>
               </td>
               <td>
                  <c:if test="${putState}">
                     <fmt:message key="key.on" bundle="${msg}"/>
                  </c:if>
                  <c:if test="${!putState}">
                     <fmt:message key="key.off" bundle="${msg}"/>
                  </c:if>
               </td>
            </tr>
            <tr>
               <td>
                  <input type="button"value="PUT" onclick="handleClick('delete');"/>
               </td>
               <td>
                  <c:if test="${deleteState}">
                     <fmt:message key="key.on" bundle="${msg}"/>
                  </c:if>
                  <c:if test="${!deleteState}">
                     <fmt:message key="key.off" bundle="${msg}"/>
                  </c:if>
               </td>
            </tr>
         </table>
      </form>
      <c:if test="${cookie['requestCount'] == null}">
         Count request = 0;
      </c:if>
      <c:if test="${cookie['requestCount'] != null}">
         Count request = ${cookie['requestCount'].value};
      </c:if>
   </body>
</html>