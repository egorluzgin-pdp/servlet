import java.util.ListResourceBundle;

public class Message extends ListResourceBundle {

    static final Object[][] contents = { { "key.on", "On" },{ "key.off", "Off" } };

    @Override
    protected Object[][] getContents() {
        return contents;
    }
}
