package servlets;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;

@WebServlet(name = "CustomerController", urlPatterns = "/action")
public class ActionServlet extends HttpServlet {

    private static final String REQUEST_COUNT = "requestCount";
    private static final String GET_STATE_ATTRIBUTE = "getState";
    private static final String POST_STATE_ATTRIBUTE = "postState";
    private static final String PUT_STATE_ATTRIBUTE = "putState";
    private static final String DELETE_STATE_ATTRIBUTE = "deleteState";
    public static final String INDEX_JSP = "/index.jsp";

    private boolean getState = false;
    private boolean postState = false;
    private boolean putState = false;
    private boolean deleteState = false;

    @Override
    public void init(ServletConfig config) throws ServletException {
        config.getServletContext().setAttribute(GET_STATE_ATTRIBUTE, getState);
        config.getServletContext().setAttribute(POST_STATE_ATTRIBUTE, postState);
        config.getServletContext().setAttribute(PUT_STATE_ATTRIBUTE, putState);
        config.getServletContext().setAttribute(DELETE_STATE_ATTRIBUTE, deleteState);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.getState = !this.getState;
        request.getSession().setAttribute(GET_STATE_ATTRIBUTE, getState);
        updateCookieAndRedirect(request, response);
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        postState = !postState;
        request.getSession().setAttribute(POST_STATE_ATTRIBUTE, postState);
        updateCookieAndRedirect(request, response);
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        putState = !putState;
        request.getSession().setAttribute(PUT_STATE_ATTRIBUTE, putState);
        updateCookieAndRedirect(request, response);
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        deleteState = !deleteState;
        request.getSession().setAttribute(DELETE_STATE_ATTRIBUTE, deleteState);
        updateCookieAndRedirect(request, response);
    }

    private void updateCookieAndRedirect(HttpServletRequest request, HttpServletResponse response) throws IOException {
        updateRequestCount(request, response);
        response.sendRedirect(request.getContextPath() + INDEX_JSP);
    }

    private void updateRequestCount(HttpServletRequest request, HttpServletResponse response) {
        int count = 1;
        Optional<Cookie> cookie = Arrays.stream(request.getCookies())
                .filter(c -> REQUEST_COUNT.equals(c.getName()))
                .findFirst();
        if (cookie.isPresent()) {
            String value = cookie.get().getValue();
            count = Integer.parseInt(value) + 1;
        }
        response.addCookie(new Cookie(REQUEST_COUNT, String.valueOf(count)));
    }
}